<?php
theme_register_sidebar('Area-2',  __('mas-vistos Widget Area', 'default'));

function theme_block_11_14($title = '', $content = '', $class = '', $id = ''){
    ob_start();
?>
    <div class="data-control-id-1513741 bd-block-11 bd-own-margins <?php echo $class; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class="data-control-id-1513778 bd-blockheader bd-tagstyles bd-bootstrap-btn bd-btn-success">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class="data-control-id-1513745 bd-blockcontent bd-tagstyles bd-bootstrap-btn bd-btn-success bd-custom-image bd-bootstrap-img bd-img-circle bd-custom-bulletlist bd-custom-orderedlist <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
    return ob_get_clean();
}
?>
<?php
function theme_block_default_1_14($title = '', $content = '', $class = '', $id = ''){
?>
    <div class="data-control-id-1002685 bd-block bd-own-margins <?php echo $class; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class="data-control-id-1002686 bd-blockheader bd-tagstyles bd-bootstrap-btn bd-btn-success">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class="data-control-id-1002718 bd-blockcontent bd-tagstyles bd-bootstrap-btn bd-btn-success <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
}
?>