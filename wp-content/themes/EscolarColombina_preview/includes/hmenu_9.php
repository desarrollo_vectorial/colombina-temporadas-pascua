<?php

register_nav_menus(array('primary-menu-9' => __('blog-mobile', 'default')));

function theme_hmenu_9() {
?>
    
    <nav class="data-control-id-1486388 bd-hmenu-9" data-responsive-menu="true" data-responsive-levels="">
        
            <div class="data-control-id-1486387 bd-responsivemenu-8 collapse-button">
    <div class="bd-container-inner">
        <div class="bd-menuitem-61 data-control-id-1486390">
            <a  data-toggle="collapse"
                data-target=".bd-hmenu-9 .collapse-button + .navbar-collapse"
                href="#" onclick="return false;">
                    <span></span>
            </a>
        </div>
    </div>
</div>
            <div class="navbar-collapse collapse">
        
        <div class="data-control-id-1486386 bd-horizontalmenu-10 clearfix">
            <div class="bd-container-inner">
            <?php
                echo theme_get_menu(array(
                    'source' => theme_get_option('theme_menu_source'),
                    'depth' => theme_get_option('theme_menu_depth'),
                    'theme_location' => 'primary-menu-9',
                    'responsive' => 'xs',
                    'responsive_levels' => '',
                    'levels' => '',
                    'popup_width' => 'sheet',
                    'popup_custom_width' => '600',
                    'columns' => array(
                        'lg' => '',
                        'md' => '',
                        'sm' => '',
                        'xs' => '',
                    ),
                    'menu_function' => 'theme_menu_9_19',
                    'menu_item_start_function' => 'theme_menu_item_start_9_56',
                    'menu_item_end_function' => 'theme_menu_item_end_9_56',
                    'submenu_start_function' => 'theme_submenu_start_9_20',
                    'submenu_end_function' => 'theme_submenu_end_9_20',
                    'submenu_item_start_function' => 'theme_submenu_item_start_9_57',
                    'submenu_item_end_function' => 'theme_submenu_item_end_9_57',
                ));
            ?>
            </div>
        </div>
        
        
            </div>
    </nav>
    
<?php
}

function theme_menu_9_19($content = '') {
    ob_start();
    ?><ul class="data-control-id-1486435 bd-menu-19 nav nav-pills navbar-right">
    <?php echo $content; ?>
</ul><?php
    return ob_get_clean();
}

function theme_menu_item_start_9_56($class, $title, $attrs, $link_class, $item_type = '') {
    if ($item_type === 'mega') {
        $class .= ' ';
    }
    ob_start();
    ?><li class="data-control-id-1486436 bd-menuitem-56 bd-toplevel-item <?php echo $class; ?>">
    <a class="<?php echo $link_class; ?>" <?php echo $attrs; ?>>
        <span>
            <?php echo $title; ?>
        </span>
    </a><?php
    return ob_get_clean();
}

function theme_menu_item_end_9_56() {
    ob_start();
?>
    </li>
    
<?php
    return ob_get_clean();
}

function theme_submenu_start_9_20($class = '', $item_type = '') {
    ob_start();
?>
    
    <div class="bd-menu-20-popup <?php if ($item_type === 'category') echo 'bd-megamenu-popup'; ?>">
    <?php if ($item_type === 'mega'): ?>
        <div class="bd-menu-20 bd-no-margins data-control-id-1486447 bd-mega-grid bd-grid-14 data-control-id-1486411 <?php echo $class; ?>">
            <div class="container-fluid">
                <div class="separated-grid row">
    <?php else: ?>
        <ul class="bd-menu-20 bd-no-margins data-control-id-1486447 <?php echo $class; ?>">
    <?php endif; ?>
<?php
    return ob_get_clean();
}

function theme_submenu_end_9_20($item_type = '') {
    ob_start();
?>
    <?php if ($item_type !== 'mega'): ?>
        </ul>
    <?php else: ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>
    
<?php
    return ob_get_clean();
}

function theme_submenu_item_start_9_57($class, $title, $attrs, $link_class, $item_type = '') {
    $class .= ' bd-sub-item';
    switch($item_type) {
        case 'category':
            $class .= ' bd-mega-item data-control-id-1486401 bd-menuitem-58';
            $class .= ' separated-item-5';
            break;
        case 'subcategory':
            $class .= ' bd-mega-item data-control-id-1486425 bd-menuitem-59';
            break;
    }
    ob_start();
?>
    
    <?php if ($item_type === 'category'): ?>
        <div class="data-control-id-1486448 bd-menuitem-57 <?php echo $class; ?>">
            <div class="data-control-id-1486413 bd-griditem-5 bd-grid-item">
    <?php else: ?>
        <li class="data-control-id-1486448 bd-menuitem-57 <?php echo $class; ?>">
    <?php endif; ?>

            <a class="<?php echo $link_class; ?>" <?php echo $attrs; ?>>
                <span>
                    <?php echo $title; ?>
                </span>
            </a>
<?php
    return ob_get_clean();
}

function theme_submenu_item_end_9_57($item_type = '') {
    ob_start();
?>
    <?php if ($item_type !== 'category'): ?>
        </li>
    <?php else: ?>
            </div>
        </div>
    <?php endif; ?>

    
<?php
    return ob_get_clean();
}