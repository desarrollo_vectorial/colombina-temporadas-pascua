<?php
function theme_block_default_1_2($title = '', $content = '', $class = '', $id = ''){
?>
    <div class="data-control-id-74926 bd-block bd-own-margins <?php echo $class; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class="data-control-id-74927 bd-blockheader bd-tagstyles bd-bootstrap-btn bd-btn-success">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class="data-control-id-74928 bd-blockcontent bd-tagstyles bd-bootstrap-btn bd-btn-success <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
}
?>