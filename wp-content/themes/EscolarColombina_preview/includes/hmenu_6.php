<?php

register_nav_menus(array('primary-menu-6' => __('nav-mobile', 'default')));

function theme_hmenu_6() {
?>
    
    <nav class="data-control-id-1486295 bd-hmenu-6" data-responsive-menu="true" data-responsive-levels="">
        
            <div class="data-control-id-1486294 bd-responsivemenu-5 collapse-button">
    <div class="bd-container-inner">
        <div class="bd-menuitem-67 data-control-id-1486297">
            <a  data-toggle="collapse"
                data-target=".bd-hmenu-6 .collapse-button + .navbar-collapse"
                href="#" onclick="return false;">
                    <span></span>
            </a>
        </div>
    </div>
</div>
            <div class="navbar-collapse collapse">
        
        <div class="data-control-id-1486293 bd-horizontalmenu-11 clearfix">
            <div class="bd-container-inner">
            <?php
                echo theme_get_menu(array(
                    'source' => theme_get_option('theme_menu_source'),
                    'depth' => theme_get_option('theme_menu_depth'),
                    'theme_location' => 'primary-menu-6',
                    'responsive' => 'xs',
                    'responsive_levels' => '',
                    'levels' => '',
                    'popup_width' => 'sheet',
                    'popup_custom_width' => '600',
                    'columns' => array(
                        'lg' => '',
                        'md' => '',
                        'sm' => '',
                        'xs' => '',
                    ),
                    'menu_function' => 'theme_menu_6_21',
                    'menu_item_start_function' => 'theme_menu_item_start_6_62',
                    'menu_item_end_function' => 'theme_menu_item_end_6_62',
                    'submenu_start_function' => 'theme_submenu_start_6_22',
                    'submenu_end_function' => 'theme_submenu_end_6_22',
                    'submenu_item_start_function' => 'theme_submenu_item_start_6_64',
                    'submenu_item_end_function' => 'theme_submenu_item_end_6_64',
                ));
            ?>
            </div>
        </div>
        
        
            </div>
    </nav>
    
<?php
}

function theme_menu_6_21($content = '') {
    ob_start();
    ?><ul class="data-control-id-1486342 bd-menu-21 nav nav-pills navbar-right">
    <?php echo $content; ?>
</ul><?php
    return ob_get_clean();
}

function theme_menu_item_start_6_62($class, $title, $attrs, $link_class, $item_type = '') {
    if ($item_type === 'mega') {
        $class .= ' ';
    }
    ob_start();
    ?><li class="data-control-id-1486343 bd-menuitem-62 bd-toplevel-item <?php echo $class; ?>">
    <a class="<?php echo $link_class; ?>" <?php echo $attrs; ?>>
        <span>
            <?php echo $title; ?>
        </span>
    </a><?php
    return ob_get_clean();
}

function theme_menu_item_end_6_62() {
    ob_start();
?>
    </li>
    
<?php
    return ob_get_clean();
}

function theme_submenu_start_6_22($class = '', $item_type = '') {
    ob_start();
?>
    
    <div class="bd-menu-22-popup <?php if ($item_type === 'category') echo 'bd-megamenu-popup'; ?>">
    <?php if ($item_type === 'mega'): ?>
        <div class="bd-menu-22 bd-no-margins data-control-id-1486354 bd-mega-grid bd-grid-15 data-control-id-1486318 <?php echo $class; ?>">
            <div class="container-fluid">
                <div class="separated-grid row">
    <?php else: ?>
        <ul class="bd-menu-22 bd-no-margins data-control-id-1486354 <?php echo $class; ?>">
    <?php endif; ?>
<?php
    return ob_get_clean();
}

function theme_submenu_end_6_22($item_type = '') {
    ob_start();
?>
    <?php if ($item_type !== 'mega'): ?>
        </ul>
    <?php else: ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>
    
<?php
    return ob_get_clean();
}

function theme_submenu_item_start_6_64($class, $title, $attrs, $link_class, $item_type = '') {
    $class .= ' bd-sub-item';
    switch($item_type) {
        case 'category':
            $class .= ' bd-mega-item data-control-id-1486308 bd-menuitem-65';
            $class .= ' separated-item-6';
            break;
        case 'subcategory':
            $class .= ' bd-mega-item data-control-id-1486332 bd-menuitem-66';
            break;
    }
    ob_start();
?>
    
    <?php if ($item_type === 'category'): ?>
        <div class="data-control-id-1486355 bd-menuitem-64 <?php echo $class; ?>">
            <div class="data-control-id-1486320 bd-griditem-6 bd-grid-item">
    <?php else: ?>
        <li class="data-control-id-1486355 bd-menuitem-64 <?php echo $class; ?>">
    <?php endif; ?>

            <a class="<?php echo $link_class; ?>" <?php echo $attrs; ?>>
                <span>
                    <?php echo $title; ?>
                </span>
            </a>
<?php
    return ob_get_clean();
}

function theme_submenu_item_end_6_64($item_type = '') {
    ob_start();
?>
    <?php if ($item_type !== 'category'): ?>
        </li>
    <?php else: ?>
            </div>
        </div>
    <?php endif; ?>

    
<?php
    return ob_get_clean();
}