<?php
function theme_block_footer_3_13($title = '', $content = '', $class = '', $id = ''){
?>
    <div class="data-control-id-1445829 bd-block-3 bd-own-margins <?php echo $class; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class="data-control-id-1445869 bd-blockheader bd-tagstyles bd-bootstrap-btn bd-btn-success">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class="data-control-id-1445833 bd-blockcontent bd-tagstyles bd-bootstrap-btn bd-btn-success <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
}
?>
<?php
theme_register_sidebar('Area-5',  __('modal Widget Area', 'default'));

function theme_block_1_13($title = '', $content = '', $class = '', $id = ''){
    ob_start();
?>
    <div class="data-control-id-1491009 bd-block bd-own-margins <?php echo $class; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class="data-control-id-1491054 bd-blockheader bd-tagstyles bd-bootstrap-btn bd-btn-success">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class="data-control-id-1491018 bd-blockcontent bd-tagstyles bd-bootstrap-btn bd-btn-success <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
    return ob_get_clean();
}
?>