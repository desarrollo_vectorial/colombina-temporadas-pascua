<?php

register_nav_menus(array('primary-menu-3' => __('Blogs', 'default')));

function theme_hmenu_3() {
?>
    
    <nav class="data-control-id-1459842 bd-hmenu-3 hidden-sm" data-responsive-menu="true" data-responsive-levels="">
        
            <div class="data-control-id-1459841 bd-responsivemenu-2 collapse-button">
    <div class="bd-container-inner">
        <div class="bd-menuitem-7 data-control-id-1459844">
            <a  data-toggle="collapse"
                data-target=".bd-hmenu-3 .collapse-button + .navbar-collapse"
                href="#" onclick="return false;">
                    <span></span>
            </a>
        </div>
    </div>
</div>
            <div class="navbar-collapse collapse">
        
        <div class="data-control-id-1459840 bd-horizontalmenu-1 clearfix">
            <div class="bd-container-inner">
            <?php
                echo theme_get_menu(array(
                    'source' => theme_get_option('theme_menu_source'),
                    'depth' => theme_get_option('theme_menu_depth'),
                    'theme_location' => 'primary-menu-3',
                    'responsive' => 'sm',
                    'responsive_levels' => '',
                    'levels' => 'one level',
                    'popup_width' => 'sheet',
                    'popup_custom_width' => '600',
                    'columns' => array(
                        'lg' => '',
                        'md' => '',
                        'sm' => '',
                        'xs' => '',
                    ),
                    'menu_function' => 'theme_menu_3_1',
                    'menu_item_start_function' => 'theme_menu_item_start_3_1',
                    'menu_item_end_function' => 'theme_menu_item_end_3_1',
                    'submenu_start_function' => 'theme_submenu_start_3_2',
                    'submenu_end_function' => 'theme_submenu_end_3_2',
                    'submenu_item_start_function' => 'theme_submenu_item_start_3_2',
                    'submenu_item_end_function' => 'theme_submenu_item_end_3_2',
                ));
            ?>
            </div>
        </div>
        
        
            </div>
    </nav>
    
<?php
}

function theme_menu_3_1($content = '') {
    ob_start();
    ?><ul class="data-control-id-1459889 bd-menu-1 nav nav-pills navbar-right">
    <?php echo $content; ?>
</ul><?php
    return ob_get_clean();
}

function theme_menu_item_start_3_1($class, $title, $attrs, $link_class, $item_type = '') {
    if ($item_type === 'mega') {
        $class .= ' ';
    }
    ob_start();
    ?><li class="data-control-id-1459890 bd-menuitem-1 bd-toplevel-item <?php echo $class; ?>">
    <a class="<?php echo $link_class; ?>" <?php echo $attrs; ?>>
        <span>
            <?php echo $title; ?>
        </span>
    </a><?php
    return ob_get_clean();
}

function theme_menu_item_end_3_1() {
    ob_start();
?>
    </li>
    
<?php
    return ob_get_clean();
}

function theme_submenu_start_3_2($class = '', $item_type = '') {
    ob_start();
?>
    
    <div class="bd-menu-2-popup <?php if ($item_type === 'category') echo 'bd-megamenu-popup'; ?>">
    <?php if ($item_type === 'mega'): ?>
        <div class="bd-menu-2 bd-no-margins data-control-id-1459901 bd-mega-grid bd-grid-1 data-control-id-1459865 <?php echo $class; ?>">
            <div class="container-fluid">
                <div class="separated-grid row">
    <?php else: ?>
        <ul class="bd-menu-2 bd-no-margins data-control-id-1459901 <?php echo $class; ?>">
    <?php endif; ?>
<?php
    return ob_get_clean();
}

function theme_submenu_end_3_2($item_type = '') {
    ob_start();
?>
    <?php if ($item_type !== 'mega'): ?>
        </ul>
    <?php else: ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>
    
<?php
    return ob_get_clean();
}

function theme_submenu_item_start_3_2($class, $title, $attrs, $link_class, $item_type = '') {
    $class .= ' bd-sub-item';
    switch($item_type) {
        case 'category':
            $class .= ' bd-mega-item data-control-id-1459855 bd-menuitem-5';
            $class .= ' separated-item-1';
            break;
        case 'subcategory':
            $class .= ' bd-mega-item data-control-id-1459879 bd-menuitem-6';
            break;
    }
    ob_start();
?>
    
    <?php if ($item_type === 'category'): ?>
        <div class="data-control-id-1459902 bd-menuitem-2 <?php echo $class; ?>">
            <div class="data-control-id-1459867 bd-griditem-1 bd-grid-item">
    <?php else: ?>
        <li class="data-control-id-1459902 bd-menuitem-2 <?php echo $class; ?>">
    <?php endif; ?>

            <a class="<?php echo $link_class; ?>" <?php echo $attrs; ?>>
                <span>
                    <?php echo $title; ?>
                </span>
            </a>
<?php
    return ob_get_clean();
}

function theme_submenu_item_end_3_2($item_type = '') {
    ob_start();
?>
    <?php if ($item_type !== 'category'): ?>
        </li>
    <?php else: ?>
            </div>
        </div>
    <?php endif; ?>

    
<?php
    return ob_get_clean();
}