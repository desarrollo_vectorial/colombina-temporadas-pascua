<?php
/*
Template Name: Escolar-Front
*/
$GLOBALS['theme_current_template_info'] = array('name' => 'escolar-front');
?>
<?php if (!defined('ABSPATH')) exit; // Exit if accessed directly
?>
<!DOCTYPE html>
<html <?php echo !is_rtl() ? 'dir="ltr" ' : ''; language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>" />
    
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <script>
    var themeHasJQuery = !!window.jQuery;
</script>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/jquery.js?ver=' . wp_get_theme()->get('Version'); ?>"></script>
<script>
    window._$ = jQuery.noConflict(themeHasJQuery);
</script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.css' ?>" />
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.js' ?>"></script>
<![endif]-->
<link class="data-control-id-9" href='//fonts.googleapis.com/css?family=Cookie:regular|Roboto:100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,900,900italic&subset=latin' rel='stylesheet' type='text/css'>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.core.js' ?>"></script>
    
    <?php wp_head(); ?>
       <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88070292-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
  
  jQuery(document).on('ready', function() {
    
    jQuery('.tg-grid-holder').ajaxComplete(function() {
      
      FB.XFBML.parse();
      
    });
    
  });
  
</script>


<link href="https://fonts.googleapis.com/css?family=Mouse+Memoirs" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i" rel="stylesheet">

<link href="<?php bloginfo('template_url') ?>/main.css" rel="stylesheet">


</head>
<?php do_action('theme_after_head'); ?>
<?php ob_start(); // body start ?>
<body <?php body_class('data-control-id-1535698 bootstrap bd-body-10 
 bd-homepage bd-pagebackground-28 bd-margins'); /*   */ ?>>
<header class="data-control-id-936270 bd-headerarea-1 bd-no-margins bd-margins">
        <section class="data-control-id-1415044 bd-section-3 bd-page-width bd-tagstyles bd-bootstrap-btn bd-btn-success " id="section3" data-section-title="">
    <div class="bd-container-inner bd-margins clearfix">
        <div class="data-control-id-1459697 bd-layoutbox-7 hidden-xs bd-page-width  bd-no-margins clearfix">
    <div class="bd-container-inner">
        <?php
    if (theme_get_option('theme_use_default_menu')) {
        wp_nav_menu( array('theme_location' => 'primary-menu-3') );
    } else {
        theme_hmenu_3();
    }
?>
	
		<?php
    ob_start();
    theme_print_sidebar('Area-7', '1_15');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-7', 'data-control-id-1491152 bd-sidebar-15 clearfix', '');
?>
    </div>
</div>
    </div>
</section>
</header>
	
		<div class="data-control-id-1535738 bd-stretchtobottom-8 bd-stretch-to-bottom" data-control-selector=".bd-contentlayout-13">
<div class="bd-contentlayout-13 bd-page-width   bd-sheetstyles data-control-id-1535732 bd-no-margins bd-margins" >
    <div class="bd-container-inner">

        <div class="bd-flex-vertical bd-stretch-inner bd-contentlayout-offset">
            
            <div class="bd-flex-horizontal bd-flex-wide bd-no-margins">
                
 <?php theme_sidebar_area_5(); ?>
                <div class="bd-flex-vertical bd-flex-wide bd-no-margins">
                    

                    <div class="data-control-id-1535730 bd-layoutitemsbox-6 bd-flex-wide bd-no-margins">
    <div class="data-control-id-1535741 bd-content-9">
    
        <?php theme_blog_4(); ?>
</div>
</div>

                    
                </div>
                
            </div>
            
        </div>

    </div>
</div></div>
	
		<footer class="data-control-id-936277 bd-footerarea-1">
    <?php if (theme_get_option('theme_override_default_footer_content')): ?>
        <?php echo do_shortcode(theme_get_option('theme_footer_content')); ?>
    <?php else: ?>
        <section class="data-control-id-1406508 bd-section-2 bd-page-width bd-tagstyles bd-bootstrap-btn bd-btn-success " id="footer" data-section-title="footer">
    <div class="bd-container-inner bd-margins clearfix">
        <div class="data-control-id-2772 bd-layoutcontainer-28 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class="data-control-id-2764 bd-columnwrapper-62 
 col-lg-3
 col-sm-4">
    <div class="bd-layoutcolumn-62 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer1", 'footer_3_13');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer1', 'data-control-id-1445830 bd-footerwidgetarea-13 clearfix', '');
?></div></div>
</div>
	
		<div class="data-control-id-1464301 bd-columnwrapper-6 
 col-lg-9
 col-sm-8">
    <div class="bd-layoutcolumn-6 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer2", 'footer_1_9');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer1', 'data-control-id-1464314 bd-footerwidgetarea-9 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		
	
		<div class="data-control-id-1445604 bd-layoutcontainer-10 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class="data-control-id-1445606 bd-columnwrapper-138 
 col-sm-12">
    <div class="bd-layoutcolumn-138 bd-column" ><div class="bd-vertical-align-wrapper"><p class="data-control-id-1446147 bd-textblock-1 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
COPYRIGHT TODOS LOS DERECHOS RESERVADOS - COLOMBINA 2016
CUSTOM_CODE;
?>
</p></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<?php
    ob_start();
    theme_print_sidebar('Area-5', '1_13');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-5', 'data-control-id-1491010 bd-sidebar-13 clearfix', '');
?>
    <?php endif; ?>
</footer>
	
		<div data-smooth-scroll data-animation-time="250" class="data-control-id-1535728 bd-smoothscroll-2"><a href="#" class="data-control-id-1535718 bd-backtotop-3 ">
    <span class="bd-icon-4 bd-icon data-control-id-1535717"></span>
</a></div>
<div id="wp-footer">
    <?php wp_footer(); ?>
    <!-- <?php printf(__('%d queries. %s seconds.', 'default'), get_num_queries(), timer_stop(0, 3)); ?> -->
</div>
</body>
<?php echo apply_filters('theme_body', ob_get_clean()); // body end ?>
</html>