<?php theme_add_template_option('Blog', 'blogTemplate', __('Blog', 'default')); ?>
<?php theme_add_template_query_option('Blog', 'blogTemplate', __('Blog', 'default')); ?>
<?php theme_add_template_option('Home', 'escolar-front', __('Escolar-Front', 'default')); ?>
<?php theme_add_template_option('Home', 'home', __('Home', 'default')); ?>
<?php theme_add_template_option('Home', 'navidad-front', __('Navidad-Front', 'default')); ?>
<?php theme_add_template_option('404', 'template404', __('404', 'default'), 0); ?>