<?php
theme_register_sidebar('Area-4',  __('LOGO Widget Area', 'default'));

function theme_block_9_10($title = '', $content = '', $class = '', $id = ''){
    ob_start();
?>
    <div class=" bd-block-9 bd-own-margins <?php echo $class; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class=" bd-blockheader bd-tagstyles bd-bootstrap-btn bd-btn-success">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class=" bd-blockcontent bd-tagstyles bd-bootstrap-btn bd-btn-success <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
    return ob_get_clean();
}
?>
<?php
function theme_block_default_5_10($title = '', $content = '', $class = '', $id = ''){
?>
    <div class=" bd-block-5 bd-no-margins bd-own-margins <?php echo $class; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class=" bd-blockheader bd-tagstyles bd-custom-blockquotes bd-custom-button bd-custom-image bd-custom-table bd-no-margins">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class=" bd-blockcontent bd-tagstyles bd-custom-blockquotes bd-custom-button bd-custom-image bd-custom-table <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
}
?>