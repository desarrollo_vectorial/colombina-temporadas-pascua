<?php
    function theme_sidebar_area_5() {
        $theme_hide_sidebar_area = true;
        ob_start();
?>
        <div data-affix
     data-offset=""
     data-fix-at-screen="top"
     data-clip-at-control="top"
     
 data-enable-lg
     
 data-enable-md
     
 data-enable-sm
     
     class=" bd-affix-2 bd-no-margins bd-margins "><section class=" bd-section-5 bd-tagstyles bd-bootstrap-btn bd-btn-success " id="nav" data-section-title="nav">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutbox-5 bd-no-margins clearfix">
    <div class="bd-container-inner">
        <?php
    ob_start();
    theme_print_sidebar('Area-4', '9_10');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-4', ' bd-sidebar-10 clearfix', '');
?>
	
		<?php
    ob_start();
    theme_print_sidebar('Area-3', '13_16');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-3', ' bd-sidebar-16 clearfix', '');
?>
	
		<?php
    ob_start();
    theme_print_sidebar('Area-2', '11_14');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-2', ' bd-sidebar-14 hidden-xs clearfix', '');
?>
    </div>
</div>
	
		<div class=" bd-layoutbox-13 hidden-md hidden-sm hidden-lg bd-no-margins clearfix">
    <div class="bd-container-inner">
        <?php
    if (theme_get_option('theme_use_default_menu')) {
        wp_nav_menu( array('theme_location' => 'primary-menu-6') );
    } else {
        theme_hmenu_6();
    }
?>
	
		<?php
    if (theme_get_option('theme_use_default_menu')) {
        wp_nav_menu( array('theme_location' => 'primary-menu-9') );
    } else {
        theme_hmenu_9();
    }
?>
    </div>
</div>
    </div>
</section></div>
        <?php $area_content = trim(ob_get_clean()); ?>

        <?php if (theme_is_preview()): ?>
            <?php $hide = 
                !strlen(trim(preg_replace('/<!-- empty::begin -->[\s\S]*?<!-- empty::end -->/', '', $area_content))); /* no other controls */ ?>

            <aside class="bd-sidebararea-5-column  bd-flex-vertical bd-flex-fixed <?php if ($hide) echo ' hidden bd-hidden-sidebar'; ?>">
                <div class="bd-sidebararea-5 bd-flex-wide  bd-margins">
                    
                    <?php echo $area_content ?>
                    
                </div>
            </aside>
        <?php else: ?>
            <?php if ($area_content): ?>
                <aside class="bd-sidebararea-5-column  bd-flex-vertical bd-flex-fixed">
                    <div class="bd-sidebararea-5 bd-flex-wide  bd-margins">
                        
                        <?php echo $area_content ?>
                        
                    </div>
                </aside>
            <?php endif; ?>
        <?php endif; ?>
<?php
    }
?>