<?php
theme_register_sidebar('Area-2',  __('mas-vistos Widget Area', 'default'));

function theme_block_11_14($title = '', $content = '', $class = '', $id = ''){
    ob_start();
?>
    <div class=" bd-block-11 bd-own-margins <?php echo $class; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class=" bd-blockheader bd-tagstyles bd-bootstrap-btn bd-btn-success">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class=" bd-blockcontent bd-tagstyles bd-bootstrap-btn bd-btn-success bd-custom-image bd-bootstrap-img bd-img-circle bd-custom-bulletlist bd-custom-orderedlist <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
    return ob_get_clean();
}
?>
<?php
function theme_block_default_1_14($title = '', $content = '', $class = '', $id = ''){
?>
    <div class=" bd-block bd-own-margins <?php echo $class; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class=" bd-blockheader bd-tagstyles bd-bootstrap-btn bd-btn-success">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class=" bd-blockcontent bd-tagstyles bd-bootstrap-btn bd-btn-success <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
}
?>