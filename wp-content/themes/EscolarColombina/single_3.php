<?php
/*
Template Name: Escolar-post
*/
$GLOBALS['theme_current_template_info'] = array('name' => 'escolar-post');
?>
<?php if (!defined('ABSPATH')) exit; // Exit if accessed directly
?>
<!DOCTYPE html>
<html <?php echo !is_rtl() ? 'dir="ltr" ' : ''; language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>" />
    
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <script>
    var themeHasJQuery = !!window.jQuery;
</script>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/jquery.js?ver=' . wp_get_theme()->get('Version'); ?>"></script>
<script>
    window._$ = jQuery.noConflict(themeHasJQuery);
</script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.css' ?>" />
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.js' ?>"></script>
<![endif]-->
<link class="" href='//fonts.googleapis.com/css?family=Cookie:regular|Roboto:100,100italic,300,300italic,regular,italic,500,500italic,700,700italic,900,900italic&subset=latin' rel='stylesheet' type='text/css'>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.core.js' ?>"></script>
    
    <?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-88070292-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-88070292-1');
    </script>
<script>
  
  jQuery(document).on('ready', function() {
    
    jQuery('.tg-grid-holder').ajaxComplete(function() {
      
      FB.XFBML.parse();
      
    });
    
  });
  
</script>


<link href="https://fonts.googleapis.com/css?family=Mouse+Memoirs" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i" rel="stylesheet">

<link href="<?php bloginfo('template_url') ?>/main.css" rel="stylesheet">


</head>
<?php do_action('theme_after_head'); ?>
<?php ob_start(); // body start ?>
<body <?php body_class(' bootstrap bd-body-3  bd-pagebackground-32 bd-margins'); /*   */ ?>>
<header class=" bd-headerarea-1 bd-no-margins bd-margins">
        <section class=" bd-section-3 bd-page-width bd-tagstyles bd-bootstrap-btn bd-btn-success " id="section3" data-section-title="">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutbox-7 hidden-xs bd-page-width  bd-no-margins clearfix">
    <div class="bd-container-inner">
        <?php
    if (theme_get_option('theme_use_default_menu')) {
        wp_nav_menu( array('theme_location' => 'primary-menu-3') );
    } else {
        theme_hmenu_3();
    }
?>
	
		<?php
    ob_start();
    theme_print_sidebar('Area-7', '1_15');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-7', ' bd-sidebar-15 clearfix', '');
?>
    </div>
</div>
    </div>
</section>
</header>
	
		<div class="bd-contentlayout-10 bd-page-width  bd-sheetstyles  bd-no-margins bd-margins" >
    <div class="bd-container-inner">

        <div class="bd-flex-vertical bd-stretch-inner bd-contentlayout-offset">
            
            <div class="bd-flex-horizontal bd-flex-wide bd-no-margins">
                
 <?php theme_sidebar_area_5(); ?>
                <div class="bd-flex-vertical bd-flex-wide bd-no-margins">
                    

                    <div class=" bd-layoutitemsbox-3 bd-flex-wide bd-no-margins">
    <div class=" bd-content-2">
    
        <?php theme_blog_8(); ?>
</div>
</div>

                    
                </div>
                
            </div>
            
        </div>

    </div>
</div>
	
		<footer class=" bd-footerarea-1">
    <?php if (theme_get_option('theme_override_default_footer_content')): ?>
        <?php echo do_shortcode(theme_get_option('theme_footer_content')); ?>
    <?php else: ?>
        <section class=" bd-section-2 bd-page-width bd-tagstyles bd-bootstrap-btn bd-btn-success " id="footer" data-section-title="footer">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-28 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-62 
 col-lg-3
 col-sm-4">
    <div class="bd-layoutcolumn-62 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer1", 'footer_3_13');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer1', ' bd-footerwidgetarea-13 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-6 
 col-lg-9
 col-sm-8">
    <div class="bd-layoutcolumn-6 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer2", 'footer_1_9');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer1', ' bd-footerwidgetarea-9 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		
	
		<div class=" bd-layoutcontainer-10 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-138 
 col-sm-12">
    <div class="bd-layoutcolumn-138 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-1 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
COPYRIGHT TODOS LOS DERECHOS RESERVADOS - COLOMBINA 2018
CUSTOM_CODE;
?>
</p></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<?php
    ob_start();
    theme_print_sidebar('Area-5', '1_13');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-5', ' bd-sidebar-13 clearfix', '');
?>
    <?php endif; ?>
</footer>
<div id="wp-footer">
    <?php wp_footer(); ?>
    <!-- <?php printf(__('%d queries. %s seconds.', 'default'), get_num_queries(), timer_stop(0, 3)); ?> -->
</div>
</body>
<?php echo apply_filters('theme_body', ob_get_clean()); // body end ?>
</html>