# Translation of Stable (latest release) in Spanish (Spain)
# This file is distributed under the same license as the Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2016-03-31 16:11:37+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/1.1.0-alpha\n"
"Project-Id-Version: Stable (latest release)\n"

#: image-widget.php:38
msgid "Showcase a single image with a Title, URL, and a Description"
msgstr "Muestra una imagen con un título, una URL, y una descripción"

#. #-#-#-#-#  tmp-image-widget.pot (Image Widget 4.1.2)  #-#-#-#-#
#. Plugin Name of the plugin/theme
#: image-widget.php:40
msgid "Image Widget"
msgstr "Widget de una imagen"

#: image-widget.php:75 views/widget-admin.php:13
msgid "Select an Image"
msgstr "Seleccione una Imagen"

#: image-widget.php:76 lib/ImageWidgetDeprecated.php:66
msgid "Insert Into Widget"
msgstr "Insertar en el Widget"

#: image-widget.php:400
msgid "Thanks for upgrading the Image Widget! If you like this plugin, please consider <a href=\"%s\" target=\"_blank\">rating it</a> and maybe even check out our premium plugins including our <a href=\"%s\" target=\"_blank\">Events Calendar Pro</a>!"
msgstr "Gracias por actualizar Image Widget! Si te gusta este plugin, por favor considera <a href=\"%s\" target=\"_blank\">Calificarlo</a> y puede que quieras echarle un vistazo a nuestros plugins premium, entre ellos nuestro <a href=\"%s\" target=\"_blank\">Events Calendar Pro</a>!"

#: image-widget.php:415
msgid "Check out our other <a href=\"%s\" target=\"_blank\">plugins</a> including our <a href=\"%s\" target=\"_blank\">Events Calendar Pro</a>!"
msgstr "Echa un vistazo a nuestros <a href=\"%s\" target=\"_blank\">plugins</a> incluyendo nuestro <a href=\"%s\" target=\"_blank\">Events Calendar Pro</a>!"

#: views/widget-admin.deprecated.php:10 views/widget-admin.php:23
msgid "Title"
msgstr "Título"

#: views/widget-admin.deprecated.php:13
msgid "Image"
msgstr "Imagen"

#: views/widget-admin.deprecated.php:17
msgid "Change Image"
msgstr "Cambiar Imagen"

#: views/widget-admin.deprecated.php:17
msgid "Add Image"
msgstr "Añadir Imagen"

#: views/widget-admin.deprecated.php:25 views/widget-admin.php:29
msgid "Caption"
msgstr "Leyenda"

#: views/widget-admin.deprecated.php:28 views/widget-admin.php:32
msgid "Link"
msgstr "Enlace"

#: views/widget-admin.deprecated.php:31 views/widget-admin.php:37
msgid "Stay in Window"
msgstr "Abrir en la misma ventana"

#: views/widget-admin.deprecated.php:32 views/widget-admin.php:38
msgid "Open New Window"
msgstr "Abrir en ventana nueva"

#: views/widget-admin.deprecated.php:35 views/widget-admin.php:68
msgid "Width"
msgstr "Ancho"

#: views/widget-admin.deprecated.php:38 views/widget-admin.php:71
msgid "Height"
msgstr "Alto"

#: views/widget-admin.deprecated.php:41 views/widget-admin.php:76
msgid "Align"
msgstr "Alinear"

#: views/widget-admin.deprecated.php:43 views/widget-admin.php:78
msgid "none"
msgstr "Ninguno"

#: views/widget-admin.deprecated.php:44 views/widget-admin.php:79
msgid "left"
msgstr "Izquierda"

#: views/widget-admin.deprecated.php:45 views/widget-admin.php:80
msgid "center"
msgstr "Centrado"

#: views/widget-admin.deprecated.php:46 views/widget-admin.php:81
msgid "right"
msgstr "Derecha"

#: views/widget-admin.deprecated.php:49 views/widget-admin.php:26
msgid "Alternate Text"
msgstr "Texto Alternativo"

#: views/widget-admin.php:34
msgid "Link ID"
msgstr "ID del enlace"

#: views/widget-admin.php:46
msgid "Size"
msgstr "Tamaño"

#: views/widget-admin.php:51
msgid "Full Size"
msgstr "Tamaño Completo"

#: views/widget-admin.php:52
msgid "Thumbnail"
msgstr "Miniatura"

#: views/widget-admin.php:53
msgid "Medium"
msgstr "Medio"

#: views/widget-admin.php:54
msgid "Large"
msgstr "Largo"

#: views/widget-admin.php:56
msgid "Custom"
msgstr "Personalizado"

#. Plugin URI of the plugin/theme
msgid "http://wordpress.org/extend/plugins/image-widget/"
msgstr "http://es.wordpress.org/extend/plugins/image-widget/"

#. Description of the plugin/theme
msgid "A simple image widget that uses the native WordPress media manager to add image widgets to your site."
msgstr "Un sencillo widget de imagen que usa la biblioteca de medios nativa de WordPress para añadir imágenes en tu sitio."

#. Author of the plugin/theme
msgid "Modern Tribe, Inc."
msgstr "Modern Tribe, Inc."

#. Author URI of the plugin/theme
msgid "http://m.tri.be/26"
msgstr "http://m.tri.be/26"
