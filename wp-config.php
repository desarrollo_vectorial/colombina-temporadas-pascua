<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
if ($_SERVER["HTTP_HOST"] == 'www.temporadascolombina.com') {
    /** El nombre de tu base de datos de WordPress */
    define('DB_NAME', 'ColombinaTemporadas');

    /** Tu nombre de usuario de MySQL */
    define('DB_USER', 'Colombina');

    /** Tu contraseña de MySQL */
    define('DB_PASSWORD', 'c0l4M81_:$8kf$l__9qie$:$');

    /** Host de MySQL (es muy probable que no necesites cambiarlo) */
    define('DB_HOST', 'dbproduccion1.cartbphlockr.us-west-2.rds.amazonaws.com');

} elseif ($_SERVER["HTTP_HOST"] == 'temporadascolombina-pascua.vectorial.co') {

    define('WP_HOME', 'http://temporadascolombina-pascua.vectorial.co/');
    define('WP_SITEURL', 'http://temporadascolombina-pascua.vectorial.co/');

    /** El nombre de tu base de datos de WordPress */
    define('DB_NAME', 'temporadas_pascua');

    /** Tu nombre de usuario de MySQL */
    define('DB_USER', 'temporadas');

    /** Tu contraseña de MySQL */
    define('DB_PASSWORD', 'T3mp0raDas');

    /** Host de MySQL (es muy probable que no necesites cambiarlo) */
    define('DB_HOST', 'localhost');

} else {

    define('WP_HOME', 'http://temporadascolombinapascua.develop/');
    define('WP_SITEURL', 'http://temporadascolombinapascua.develop/');

    /** El nombre de tu base de datos de WordPress */
    define('DB_NAME', 'temporadas_pascua');

    /** Tu nombre de usuario de MySQL */
    define('DB_USER', 'root');

    /** Tu contraseña de MySQL */
    define('DB_PASSWORD', 'root');

    /** Host de MySQL (es muy probable que no necesites cambiarlo) */
    define('DB_HOST', 'localhost');

}

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '&0dS>`<NqrpJC>CJgjd&r|?wFZS([ )pyT2h8v:6UwJQU=XaxuehOFp7aB;>vcRJ');
define('SECURE_AUTH_KEY', 'N GObI-T~ypEM?%5g=PSy{Ej0CUs>kK2I+8i,qOzP!5;|U&(nhjvQ@8FH`Kbcr0r');
define('LOGGED_IN_KEY', 'bXiZ:&Dt2N!p@rL^0A:%eRvMtzBc]zZ3#de?v^I}mZrPm7Z!S#;O].&==czAL8<Z');
define('NONCE_KEY', 'M1m $Q+l(TE~qZcW~t;=nmc{(MD5,jF01Q0zLatd0`NX{}9^(4wTQ)BJs39AyYT=');
define('AUTH_SALT', '(`(T]BajM?Bi#zjSz`S3vZ7(}#]6Bxhy~Q:+[I7ilt;o!:|H_ONk;D _0`~emY2h');
define('SECURE_AUTH_SALT', 'v^0m$&jX~5US]Jr2)i~Y>([Q6%7MK$2OxQZ00Y=*((P>Dz}]WXzb$Q%G8LYpCnRn');
define('LOGGED_IN_SALT', '2pK#$s^T)f@YkO/9&fr,$C%f2`-U7{ZyOR:u?YsNQ^C8JziERsQB39K7}920&*TY');
define('NONCE_SALT', ')O7{A BJyf!U{:T@C5g(cg!z.V 0WNmw;1h!u!I##^9Hs4a82mOtNN#o[muAF![B');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix = 'wppascua_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if (!defined('ABSPATH'))
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

